package com.j2core.sts.stsmathexpression;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class ExpressionCalculationTest{

    @Test
    public void testCalculationStringExpressionPositiveCases(){

        String[] testExpressions = new String[] {
                ".4+2",
                " xy^2++7 -(5^2/2.5)",
                "(+4*x^2)",
                "5xy^0+(0.4-+5^2)*z/-04+(2y-5)"
        };
        Double[][] testValueVariable = new Double[][]{
                {1.1, 1.1, 1.1},
                {4.2, 1.5, 2.0},
                {3.0, 1.5, 2.0},
                {4.0, 1.0, 2.0}
        };
        Double[] testResult = new Double[]{ 2.4, 36.69, 36.0, 10.3};
        ExpressionCalculation expressionCalculation = new ExpressionCalculation();

        try {
            for (int i = 0; i < testExpressions.length; i++) {
                Assert.assertEquals(expressionCalculation.calculationStringExpression(testExpressions[i], testValueVariable[i][0], testValueVariable[i][1], testValueVariable[i][2]), testResult[i], 0.000001);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
