package com.j2core.sts.stsmathexpression;

/**
 * Created by sts on 8/27/15.
 */

import java.util.List;

/**
 * The class is calculating math action with numbers from Lists
 */
public class CalculationMathAction {

    /**
     * The method is calculating actions from action's List with number's List
     *
     * @param action List with math actions
     * @param numbers List with numbers for calculation
     * @return result calculation
     */
    public static Double calculation(List<Character> action, List<Double> numbers) throws ArithmeticException{

        int index_availability;
        Double data1, data2, sum;
        final double INACCURACY = 0.000001;

        // Performance all operations of '/'
        index_availability = action.lastIndexOf('/');

        while (index_availability > -1) {
            data1 = numbers.get(index_availability);
            data2 = numbers.get(index_availability + 1);
            //  Check division by zero
            if (data2 > 0.0 && data2 <= INACCURACY) {
                throw new ArithmeticException(" Sorry! Division by zero is inadmissible.");
            } else {
                numbers.remove(index_availability + 1);
                sum = data1 / data2;
                numbers.set(index_availability, sum);
                action.remove(index_availability);
            }

            index_availability = action.lastIndexOf('/');
        }

        // Performance all operations of '*'
        index_availability = action.lastIndexOf('*');

        while (index_availability > -1) {
            data1 = numbers.get(index_availability);
            data2 = numbers.get(index_availability + 1);
            numbers.remove(index_availability + 1);
            sum = data1 * data2;
            numbers.set(index_availability, sum);
            action.remove(index_availability);

            index_availability = action.lastIndexOf('*');
        }

        // Performance all operations of '+' and '-'
        while (action.size() > 0) {
            data1 = numbers.get(0);
            data2 = numbers.get(1);
            numbers.remove(1);
            if ('+' == action.get(0)) {
                sum = data1 + data2;
            } else {
                sum = data1 - data2;
            }
            numbers.set(0, sum);
            action.remove(0);
        }

        return numbers.get(0);
    }


    /**
     * The method involution numbers
     *
     * @param number  number for involution
     * @param expression math expression
     * @return  number's value after involution
     * @throws Exception if power of number is not integer
     */
    public static Double involutionNumbers(Double number, String expression) throws Exception {

        int j = 0;
        int i = 0;
        int powerOfNumber;
        double result = number;

        while (i < expression.length() && Character.isDigit(expression.charAt(i))) {
            i++;
        }
        if ((i - j) == 0) throw new Exception("Can't get valid number in '" + expression + "'");

        if (i < expression.length()) {
            powerOfNumber = Integer.parseInt(expression.substring(j, i));
        } else powerOfNumber = Integer.parseInt(expression);

        if (powerOfNumber == 0) {
            result = 1.0;

        } else {
            for (int l = 1; l < Math.abs(powerOfNumber); l++) {
                result = result * number;
            }
            if (powerOfNumber < 0) {
                result = 1 / result;
            }
        }
        return result;
    }
}
