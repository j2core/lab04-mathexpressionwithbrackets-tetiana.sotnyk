package com.j2core.sts.stsmathexpression;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This project create for calculation expression
 */
public class MainMathExpression
{
    public static void main( String[] args ) {

        Scanner scanner = new Scanner(System.in);
        String expression = null;
        Double x;
        Double y;
        Double z;

        System.out.println(" Please enter your expression and value variable! " + System.lineSeparator() + " Warning: " +
                "expression can contain integers, numbers with floating point, brackets, variables(only three: x; y; z), and action +, -, *, /."
                + System.lineSeparator() + " Your expression is ");

        // Input String expression from console
        while (expression == null){
            expression = scanner.nextLine();

            if (!ExpressionVerification.isExpressionValid(expression)){
                expression = null;
                System.out.println(" Sorry! This expression isn't correcting. Try again!");
            }
        }
        // Input Double value for variable
        System.out.println(" Enter your value for variable. ");
        System.out.println(" Value for X = ");
        x = inputValueVariable(scanner);
        System.out.println(" Value for Y = ");
        y = inputValueVariable(scanner);
        System.out.println(" Value for Z = ");
        z = inputValueVariable(scanner);

        ExpressionCalculation expressionCalculation = new ExpressionCalculation();

        // Output result calculation expression
        try {
            Double result = expressionCalculation.calculationStringExpression(expression, x, y, z);
            System.out.println(" Result calculation expression =  " + result);

        } catch (ArithmeticException ex) {
            System.out.println(" Sorry! Division by zero is inadmissible." + ex.getMessage());
        } catch (Exception ex) {
            System.out.println(" Sorry! Data isn't validation!" + ex.getMessage());
        }

    }


    /**
     * This method inquire value variable from console
     *
     * @param scanner object class's Scanner
     * @return value variable
     */
    public static Double inputValueVariable(Scanner scanner){

        Double valueVariable = null;
        while (valueVariable == null) {
            try {
                valueVariable = scanner.nextDouble();
            } catch (InputMismatchException ex) {
                System.out.println(" This number is not correct. Try again.");
            }
        }
        return valueVariable;
    }
}
