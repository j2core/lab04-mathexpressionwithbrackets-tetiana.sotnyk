package com.j2core.sts.stsmathexpression;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by sts on 9/17/15.
 */
public class ExpressionUtilTest {

    @Test
    public void testTrimSpace(){

        String[] testData = {
                "5 + 43 - x ",
                " (54/ 2. 04* -3) - 45",
                "34+ 0 . 045- 32/- 45. 08"
        };

        for (String data : testData){
            String testExpression = ExpressionUtil.trimSpace(data);
            Assert.assertEquals(testExpression.lastIndexOf(" "), -1);
        }
    }

    @Test
    public void testTrimBrackets(){

        String[] testData = {
                "(5-2/45-(-5-2))",
                "(54-23/7)+(45+34*0.32)"
        };

        String[] correctValue = {
                "5-2/45-(-5-2)",
                "(54-23/7)+(45+34*0.32)"
        };

        for (int i = 0; i < testData.length; i++){
            Assert.assertEquals(ExpressionUtil.trimBrackets(testData[i]), correctValue[i]);
        }

    }
}
