package com.j2core.sts.stsmathexpression;

/**
 * Created by sts on 9/3/15.
 */

/**
 * The class trim superfluous brackets and free spaces
 */
public class ExpressionUtil {


    /**
     * The method trim superfluous brackets in expression
     *
     * @param expression  user's expression
     * @return   expression after trim superfluous brackets
     */
    public  static String trimBrackets(String expression){

        String newExpression =  expression.substring(1, (expression.length() - 1));

        return ExpressionVerification.isBracketsValid(newExpression) ? newExpression : expression;
    }


    /**
     * The method trim superfluous free spaces in expression
     *
     * @param expression   user's expression
     * @return    expression after trim spaces
     */
    public static String trimSpace(String expression){

        StringBuilder newExpression = new StringBuilder(expression);

        int index = newExpression.lastIndexOf(" ");
        while (index > -1){

            newExpression.deleteCharAt(index);
            index = newExpression.lastIndexOf(" ");
        }

        return newExpression.toString();
    }
}
