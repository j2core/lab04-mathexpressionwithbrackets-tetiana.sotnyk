package com.j2core.sts.stsmathexpression;

/**
 * Created by sts on 8/24/15.
 */

/**
 * This class is designed to validate the expression
 * It's check correctness of the brackets, fractional numbers,
 * as well as the absence of a division by zero and letters, except permitted variables.
 */
public class ExpressionVerification {

    static final String  validSymbol = "().+-*/^xXyYzZ";

    /**
     * The method is verification check correctness of the brackets in expression
     *
     * @param expression Expression for verification
     * @return  is brackets valid in expression
     */
    public static boolean isBracketsValid (String expression){

        int bracketBalance = 0;

        for (int i = 0; i < expression.length(); i++) {
            if (expression.charAt(i) == '(') {
                bracketBalance++;
            }
            else if (expression.charAt(i) == ')') {
                bracketBalance--;
            }
            if(bracketBalance<0) return false;
        }
        return (bracketBalance == 0);
    }


    /**
     * The method is verification presence of extra points in expression
     *
     * @param expression Expression for verification
     * @return  are valid numbers in expression
     */
    public static boolean isNumbersValid(String expression){

        int index = 0;
        while (index < expression.length()) {
            if (expression.charAt(index) == '-') {
                index++;
            }

            int point = 0;
            while (index < expression.length() && (Character.isDigit(expression.charAt(index)) || expression.charAt(index) == '.')) {

                if (expression.charAt(index) == '.' && ++point > 1) {
                    return false;
                }
                index++;
            }
            index++;
        }
        return true;

    }


    /**
     * The method is verification presence of letters in expression, except permitted variables
     *
     * @param expression Expression for verification
     * @return  is characters valid in expression
     */
    public static boolean  isCharacterValid(String expression){

        int i = 0;
        while (i < expression.length()){
            if (Character.isDigit(expression.charAt(i))){
                i++;
            }
            else {
                if (validSymbol.indexOf(expression.charAt(i)) > -1){
                    i++;
                }else return false;
            }
        }

        return true;
    }


    /**
     * The method is executing on the all verification for expression
     *
     * @param expression Expression for verification
     * @return  is valid expression
     */
    public static boolean isExpressionValid (String expression){

        if (!isNumbersValid(expression)){
            System.out.println(" This expression is not verification. The expression have not correct number(s). A number have two points");
            return false;
        }
        if (!isCharacterValid(expression)){
            System.out.println(" This expression is not verification. The expression have not permitted letter(s)");
            return false;
        }
        if (!isBracketsValid(expression)){
            System.out.println(" This expression is not verification. The expression have not correct brackets");
            return false;
        }
        return true;
    }

}
