package com.j2core.sts.stsmathexpression;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by sts on 9/17/15.
 */
public class ExpressionVerificationTest {

    @Test
    public void testIsCharacterValid(){

        String[] testDataTru = {
                "xy+34^3/(34-z)",
                "2xY+5^z",
                "4^Y-43/34*43^Z"
        };
        String[] testDataFalse = {
                "3r+d23-43/32",
                "5<23/54",
                "[5-43^2]"
        };

        for(String data:testDataTru){
            Assert.assertTrue(ExpressionVerification.isCharacterValid(data));
        }
        for (String data:testDataFalse){
            Assert.assertFalse(ExpressionVerification.isCharacterValid(data));
        }

    }


    @Test
    public void testIsBracketsValid(){

        String[] testDataTru = {
                "(3-5/23)+((56-2)-45)",
                "((4-2)*(4+23))",
                "(3/2)-(23+4.23)-(((56+22)/2)*34)",
                "(3/5)/(34+(5-(5^2)))"
        };
        String[] testDataFalse = {
                ")4-5(5-34(67/2)",
                "(45/2)-)3/1(",
                "4/(45+(5-4)",
                "((5+34)-45/23"
        };

        for(String data:testDataTru){
            Assert.assertTrue(ExpressionVerification.isBracketsValid(data));
        }
        for (String data:testDataFalse){
            Assert.assertFalse(ExpressionVerification.isBracketsValid(data));
        }

    }


    @Test
    public void testIsNumbersValid(){

        String[] testDataTru = {
                "5.34-45/2*.04",
                "65/(4.23-1.004)/4.34",
        };
        String[] testDataFalse = {
                "4.34.5-23/(5-3)",
                "-3.4-5+(.56.34+)"
        };

        for(String data:testDataTru){
            Assert.assertTrue(ExpressionVerification.isExpressionValid(data));
        }
        for (String data:testDataFalse){
            Assert.assertFalse(ExpressionVerification.isExpressionValid(data));
        }

    }


    @Test
    public void testIsExpressionValid(){

        String[] testDataTru = {
                "5^x+45/4-(45xz*1)",
                "7^z*23/5-(45-(34.34/2)-34)",
                "(4X^z-4)/(.0043+34)"
        };
        String[] testDataFalse = {
                "(45/2))-(45.65^5)",
                ".4.54+34/4*5",
                "4s-45xy"
        };

        for(String data:testDataTru){
            Assert.assertTrue(ExpressionVerification.isExpressionValid(data));
        }
        for (String data:testDataFalse){
            Assert.assertFalse(ExpressionVerification.isExpressionValid(data));
        }

    }

}
