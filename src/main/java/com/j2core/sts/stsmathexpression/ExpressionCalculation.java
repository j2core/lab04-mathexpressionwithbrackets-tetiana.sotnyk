package com.j2core.sts.stsmathexpression;

/**
 * Created by sts on 8/24/15.
 */

import java.util.ArrayList;

/**
 * The class is calculation expression with transfer value variable
 */
public class ExpressionCalculation {

    static final String variableSymbol = "xXyYzZ";

    /**
     * The method parse and calculation string with expression
     *
     * @param expression String with expression
     * @param x value variable x
     * @param y value variable y
     * @param z value variable z
     * @return result calculation expression
     */
   public Double calculationStringExpression(String expression, Double x, Double y, Double z) throws Exception {

       ArrayList<Double> numbers = new ArrayList<>();
       ArrayList<Character> actions = new ArrayList<>();
       boolean negative = false;
       int i = 0;
       Double value;

       if (expression.lastIndexOf(" ") > -1){
           expression = ExpressionUtil.trimSpace(expression);
       }

       if (expression.charAt(0) == '(' && expression.charAt(expression.length()-1) == ')'){
           expression = ExpressionUtil.trimBrackets(expression);
       }

       while (i < expression.length() && (!(expression.charAt(i) == ')'))) {

               // if char '(' - start method with new data
               if (expression.charAt(i) == '(') {
                   value = calculationStringExpression(expression.substring(i + 1), x, y, z);
                   i = expression.indexOf(")", i) + 1;
               }
               else {
                   if (expression.charAt(i) == '-') {
                       negative = true;
                       i++;
                   }
                   if (expression.charAt(i) == '+'){
                       i++;
                   }

                   // Parser value if first char is number
                   if (Character.isDigit(expression.charAt(i)) || expression.charAt(i) == '.') {
                       int j = i;
                       while (i < expression.length() && (Character.isDigit(expression.charAt(i)) || expression.charAt(i) == '.')) {
                           i++;
                       }

                       value = Double.parseDouble(expression.substring(j, i));
                   }

                   // Set value for variable
                   else {
                       value = receiveValueVariable(expression.charAt(i), x, y, z);
                       i++;
                   }

                   while (i < expression.length()) {
                       if (variableSymbol.indexOf(expression.charAt(i)) > -1) {
                           value = value * receiveValueVariable(expression.charAt(i), x, y, z);
                           i++;
                       } else if (expression.charAt(i) == '^') {
                           i++;
                           value = CalculationMathAction.involutionNumbers(value, expression.substring(i));

                           while ( i < expression.length() && Character.isDigit(expression.charAt(i))) {
                               i++;
                           }
                       } else break;
                   }

                   // if number is negative - set value to negative
                   if (negative) {
                       value = -value;
                       negative = false;
                   }
               }

               // added number's value in Number's List
               numbers.add(value);

               // added action in Action's List
               if (i < expression.length() && (!(expression.charAt(i) == ')'))) {
                   actions.add(expression.charAt(i));
                   i++;
               }
       }

       // Calculated result math action with numbers and return result
       return CalculationMathAction.calculation(actions, numbers);
   }


    /**
     * The method is getting value handed variable
     *
     * @param symbol variable
     * @param x value variable x
     * @param y value variable y
     * @param z value variable z
     * @return  variable's value
     */
    public Double receiveValueVariable(char symbol, double x, double y, double z){

        switch (symbol){
            case 'x':
            case 'X':
                return x;

            case 'y':
            case 'Y':
                return y;

            case 'z':
            case 'Z':
                return z;

            default:
                return null;
        }

    }

}
